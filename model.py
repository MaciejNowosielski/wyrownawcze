import numpy as np
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Flatten
from tensorflow.keras.optimizers import Adam
from rl.agents import DQNAgent
from rl.policy import BoltzmannQPolicy
from rl.memory import SequentialMemory
from elements import *


def build_model(layers, moves):
    model = Sequential()
    model.add(Dense(24, activation='relu', input_shape=layers))
    model.add(Dense(24, activation='relu'))
    model.add(Dense(moves, activation='linear'))
    return model


def build_agent(model, moves):
    policy = BoltzmannQPolicy()
    memory = SequentialMemory(limit=50000, window_length=1)
    dqn = DQNAgent(model=model, memory=memory, policy=policy,
                   nb_actions=moves, nb_steps_warmup=10, target_model_update=1e-2)
    return dqn
