from model import *
from elements import *

myfont = pygame.font.SysFont('Arial', 30)

move_able = False

layers = starship.observation_space.shape
moves = starship.action_space.n




model = build_model(layers, moves)
dqn = build_agent(model, moves)
dqn.compile(Adam(lr=1e-3), metrics=['mae'])

# Ta linia wyrzuca błąd
dqn.fit(starship, nb_steps=1000, visualize=False, verbose=1)

episodes = 10

for episode in range(1, episodes + 1):

    for i, torpedo in enumerate(torpedos):
        torpedo.teleport(torpedos_Y[i][1])

    starship.y, starship.hitbox, starship.ship_alive = starship.reset()  # , starship.src
    done = False
    end = False
    score = 0

    while True and not done and not end:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                exit()

        # first layer
        screen.blit(src1, (0, 0))

        if move_able:
            action = starship.action_space.sample()
            n_state, reward, done = starship.step(action)
            score += reward
            move_able = False

        # texts
        text1 = 'Score: {}'.format(int(score))
        screen.blit(myfont.render('Episode:{} Score:{} Y:{}'.format(episode, score, starship.y), False, (0, 0, 0)),
                    (10, 0))

        # starship
        screen.blit(living_ship, (starship.x, starship.y))

        for torpedo in torpedos:
            torpedo.move(-20, 0)
            torpedo.draw()

            if torpedo.hitbox.colliderect(starship.hitbox) and starship.ship_alive:
                starship.death()

            if torpedo.base_x == 300:
                move_able = True
            if torpedo.base_x < 0:
                end = True
            else:
                end = False

        pygame.display.update()
        clock.tick(30)
