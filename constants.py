import pygame
import os

# constants
WINDOW_SIZE = (800, 600)
BACKGROUND_SIZE = (3880, 600)
WINDOW_POSITION = (100, 100)
FLAGS = 5

FONT_SIZE = 24
FONT_BOLD = False

#### setup & initialization ####

# window position
os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % WINDOW_POSITION


SCREEN = pygame.display.set_mode(WINDOW_SIZE)
SCREEN.fill(000000)

# create background
# start pygame
pygame.init()

# setting window size
screen = pygame.display.set_mode(WINDOW_SIZE, FLAGS)

pygame.display.set_caption(os.path.basename(__file__) + " in " + os.getcwd())

# getting default font
font = pygame.font.SysFont(pygame.font.get_default_font(), FONT_SIZE)
font.set_bold(FONT_BOLD)

living_ship = pygame.image.load('graphics/Ship.png')
dead_ship = pygame.image.load('graphics/Destroyed_Ship.png')


clock = pygame.time.Clock()
