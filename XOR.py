import numpy as np


# funkcja aktywacji
def activ_fun(x):
    return 1/(1+np.exp(-x))


# pochodna funkcji aktywacji
def activ_fun_derivative(x):
    s = activ_fun(x)
    return s*(1-s)

# funkcja, która oblicza wynik każdej warstwy z podanymi danymi wejściowymi,
# i wagami oraz oblicza końcowy wynik bramki XOR
def forward_propagate(data, w1, w2):
    # dodajemy do danych wejściowych kolumnę wag
    input_val = np.c_[np.ones(data.shape[0]), data]
    # wagi pierwszej warstwy są mnożone przez dane wejściowe pierwszej warstwy
    activation1 = input_val.dot(w1)
    # dane wejściowe drugiej warstwy to dane wyjściowe pierwszej warstwy,
    # które przeszły przez funkcję aktywacji wraz z dodaną kolumną
    output1 = np.c_[np.ones(data.shape[0]), activ_fun(activation1)]
    # dane wejściowe drugiej wartswy są mnożone przez wagi
    activation2 = output1.dot(w2)
    # wynik przechodzi przez funkcję aktywacji w celu uzyskania ostatecznego prawdopodobieństwa
    result = activ_fun(activation2)
    return input_val, activation1, output1, activation2, result


X = np.array([[0, 0], [0, 1], [1, 0], [1, 1]])
Y = np.array([[0, 1, 1, 0]]).T


m = X.shape[0]
n = X.shape[1]
hidden_neurons = 2
learning_rate = 1

weights1 = (np.random.random((n + 1, hidden_neurons)))
weights2 = (np.random.random((hidden_neurons + 1, 1)))

for i in range(1000):
    # dla każdej iteracji przeprowadzana jest propagacja przednia
    t1, x1, t2, x2, res = forward_propagate(X, weights1, weights2)
    # obliczany jest błąd ostatniej warstwy
    del_2 = Y - res
    del_1 = del_2.dot(weights2[1:, :].T)

    delta2 = del_2
    delta1 = del_1 * activ_fun_derivative(x1)
    # parametry są aktualizowane
    weights2 += learning_rate * t2.T.dot(delta2)
    weights1 += learning_rate * t1.T.dot(delta1)
# propagacja do przodu ze zaktualizowanymi wagami
res = forward_propagate(X, weights1, weights2)[4]

print("\nWynik\n", res)

