from constants import *
from gym import Env
from gym.spaces import Discrete, Box
import numpy as np


class Player(Env):
    def __init__(self):
        # self.src = living_ship
        self.x = 0
        self.y = 300
        self.hitbox = pygame.Rect(self.x, self.y,
                                  170, 60)
        self.ship_alive = True
        # Actions we can take, down, stay, up
        self.action_space = Discrete(3)
        # Y cord array
        # self.observation_space = Box(low=0, high=WINDOW_SIZE[1], shape=(3,))
        self.observation_space = Box(low=np.array([0]), high=np.array([WINDOW_SIZE[1]]))
        # Set game length
        self.game_length = 1800

    def step(self, action):
        # Apply action
        # action in [0, 1, 2]
        self.y += (action - 1)*200
        self.y = self.y % 600
        self.hitbox = pygame.Rect(self.x, self.y,
                                  170, 60)
        # Reduce game length by 1 second
        self.game_length -= 1

        # Calculate reward
        if self.ship_alive:
            reward = 1
        else:
            reward = -1

            # Check if shower is done
        if self.game_length <= 0 or not self.ship_alive:
            done = True
        else:
            done = False

        # Apply temperature noise
        # self.state += random.randint(-1,1)
        # Set placeholder for info
        # info = {}

        # Return step information
        return self.y, reward, done  # , info

    def draw(self):
        screen.blit(self.src, (self.x, self.y))

    def death(self):
        self.ship_alive = False
        # self.src = dead_ship

    def reset(self):
        # Reset shower temperature
        self.y = 300
        self.ship_alive = True
        # self.src = living_ship
        self.hitbox = pygame.Rect(self.x, self.y,
                                  170, 60)
        # Reset shower time
        self.game_length = 300
        return self.y, self.hitbox, self.ship_alive  # , self.src


class Ship:
    def __init__(self, source, x, y):
        self.src = pygame.image.load(source)
        self.base_x = x
        self.base_y = y
        self.hitbox = pygame.Rect(self.base_x, self.base_y,
                                  self.src.get_width(),
                                  self.src.get_height())
        self.ship_alive = True

    def draw(self):
        screen.blit(self.src, (self.base_x, self.base_y))

    def move(self, x, y):
        if self.ship_alive:
            self.base_x += x
            self.base_y += y
            self.hitbox = pygame.Rect(self.base_x, self.base_y,
                                      self.src.get_width(),
                                      self.src.get_height())

    def teleport(self, x):
        self.base_x = x
        self.hitbox = pygame.Rect(self.base_x, self.base_y,
                                  self.src.get_width(),
                                  self.src.get_height())

    def death(self):
        self.ship_alive = False
        self.src = pygame.image.load('graphics/Destroyed_Ship.png')
